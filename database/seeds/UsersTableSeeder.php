<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'rejohn@mail.com')->first();

        if (!$user) {
            User::create([
                'name'      => 'Rejohn',
                'email'     => 'rejohn@mail.com',
                'role'      => 'admin',
                'password'  => Hash::make('123456789')
            ]);
        }
    }
}
